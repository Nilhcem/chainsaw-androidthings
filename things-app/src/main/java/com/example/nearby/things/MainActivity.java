package com.example.nearby.things;

import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

public class MainActivity extends LifecycleActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Chainsaw chainsaw = new Chainsaw();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLifecycle().addObserver(chainsaw);

        new NearbyLiveData(getApplicationContext()).observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Log.e(TAG, "onChanged: " + s);

                if ("START".equals(s)) {
                    chainsaw.start();
                } else {
                    chainsaw.stop();
                }
            }
        });
    }
}
