package com.example.nearby.things;

import android.animation.ValueAnimator;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.graphics.Color;
import android.graphics.Path;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.PathInterpolator;

import com.google.android.things.contrib.driver.apa102.Apa102;

import java.io.IOException;

public class Chainsaw implements LifecycleObserver {

    private static final String TAG = Chainsaw.class.getSimpleName();
    private static final int NB_FRAMES = 4;

    private static final String SPI_PORT = "SPI0.0";
    private static final int NUM_LEDS = 60;
    private static final int LED_BRIGHTNESS = 6; // 0 ... 31
    private static final Apa102.Mode LED_MODE = Apa102.Mode.BGR;

    private Apa102 mLedstrip;
    private Handler mHandler = new Handler();
    private HandlerThread mPioThread;

    private int mFrame;
    private ValueAnimator mFrameDelayAnimator;
    private ValueAnimator mColorAnimator;

    private Runnable mAnimateRunnable = new Runnable() {

        @Override
        public void run() {
            mFrame = (mFrame + 1) % NB_FRAMES;
            mHandler.postDelayed(mAnimateRunnable, (int) mFrameDelayAnimator.getAnimatedValue());
        }
    };

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate() {
        Log.d(TAG, "APA102 MainActivity created");

        mPioThread = new HandlerThread("pioThread");
        mPioThread.start();
        mHandler = new Handler(mPioThread.getLooper());

        try {
            Log.d(TAG, "Initializing LED strip");
            mLedstrip = new Apa102(SPI_PORT, LED_MODE);
            mLedstrip.setBrightness(LED_BRIGHTNESS);
            mLedstrip.write(new int[NUM_LEDS]);
        } catch (IOException e) {
            Log.e(TAG, "Error initializing LED strip", e);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void onDestroy() {
        stop();
        mPioThread.quitSafely();
        Log.d(TAG, "Closing LED strip");
        try {
            mLedstrip.close();
        } catch (IOException e) {
            Log.e(TAG, "Exception closing LED strip", e);
        } finally {
            mLedstrip = null;
        }
    }

    public void start() {
        Path path = new Path();
        path.moveTo(0f, 0f);
        path.lineTo(0.001f, 400f / 2000f);
        path.lineTo(3f / 23f, 400f / 2000f);
        path.lineTo(6f / 23f, 20f / 2000f);
        path.lineTo(18f / 23f, 20f / 2000f);
        path.lineTo(1f, 1f);

        mFrameDelayAnimator = ValueAnimator.ofInt(0, 2000);
        mFrameDelayAnimator.setDuration(23_000);
        mFrameDelayAnimator.setRepeatCount(0);
        mFrameDelayAnimator.setInterpolator(new PathInterpolator(path));
        mFrameDelayAnimator.start();

        mColorAnimator = ValueAnimator.ofFloat(0, 360);
        mColorAnimator.setDuration(8_000);
        mColorAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mColorAnimator.setRepeatMode(ValueAnimator.REVERSE);
        mColorAnimator.start();
        mColorAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                final float[] hsv = {(float) mColorAnimator.getAnimatedValue(), 1f, 1f};

                int[] leds = new int[NUM_LEDS];
                for (int curLed = 0; curLed < NUM_LEDS; curLed++) {
                    leds[curLed] = ((curLed + mFrame) % NB_FRAMES) == 0 ? Color.HSVToColor(0, hsv) : Color.BLACK;
                }
                writeToLedStrip(leds);
            }
        });

        mHandler.post(mAnimateRunnable);
    }

    public void stop() {
        mFrameDelayAnimator.end();
        mColorAnimator.end();
        mHandler.removeCallbacks(mAnimateRunnable);
        writeToLedStrip(new int[NUM_LEDS]);
    }

    private void writeToLedStrip(int[] leds) {
        try {
            mLedstrip.write(leds);
        } catch (IOException e) {
            Log.e(TAG, "Error while writing to LED strip", e);
        }
    }
}
