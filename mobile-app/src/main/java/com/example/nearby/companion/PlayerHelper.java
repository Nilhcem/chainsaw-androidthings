package com.example.nearby.companion;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;

public class PlayerHelper implements LifecycleObserver {

    private static final Uri AUDIO_URI = Uri.parse("asset:///audio/chainsaw.mp3");

    private Context mContext;
    private ExoPlayer mExoPlayer;

    public PlayerHelper(Context context) {
        mContext = context;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate() {
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(mContext, new DefaultTrackSelector(new DefaultBandwidthMeter()));
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void onDestroy() {
        mExoPlayer.release();
    }

    public void play() {
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mContext, "ChainsawUserAgent");
        ExtractorMediaSource audioSource = new ExtractorMediaSource(AUDIO_URI, dataSourceFactory, new DefaultExtractorsFactory(), null, null);
        mExoPlayer.prepare(audioSource);
        mExoPlayer.setPlayWhenReady(true);
    }

    public void stop() {
        mExoPlayer.stop();
    }
}
