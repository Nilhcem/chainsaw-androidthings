package com.example.nearby.companion;

import android.arch.lifecycle.LifecycleActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends LifecycleActivity {

    private Button mButton;
    private NearbyHelper mNearbyHelper;
    private PlayerHelper mPlayerHelper;

    private boolean isStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNearbyHelper = new NearbyHelper(getApplicationContext());
        mPlayerHelper = new PlayerHelper(getApplicationContext());

        getLifecycle().addObserver(mNearbyHelper);
        getLifecycle().addObserver(mPlayerHelper);
        initLayout();
    }

    private void initLayout() {
        setContentView(R.layout.activity_main);
        mButton = findViewById(R.id.nearby_button);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    mNearbyHelper.sendMessage("STOP");
                    mPlayerHelper.stop();
                    mButton.setText(R.string.start);
                    isStarted = false;
                } else {
                    mNearbyHelper.sendMessage("START");
                    mPlayerHelper.play();
                    mButton.setText(R.string.stop);
                    isStarted = true;
                }
            }
        });
    }
}
